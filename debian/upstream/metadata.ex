# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/ppsspp/issues
# Bug-Submit: https://github.com/<user>/ppsspp/issues/new
# Changelog: https://github.com/<user>/ppsspp/blob/master/CHANGES
# Documentation: https://github.com/<user>/ppsspp/wiki
# Repository-Browse: https://github.com/<user>/ppsspp
# Repository: https://github.com/<user>/ppsspp.git
